===========================================================================
Primary purpose         : Single play
===========================================================================
Title                   : 10 monsters
Filename                : 10M
Release date            : 17/6/21 (dd/m/yy)
Author                  : Ilya Lazarev (joe-ilya)
Email Address           : [redacted]
Other Files By Author   : Too many
Misc. Author Info       : Most of these maps are from 2015, MAP14 is from
                          2016.

Description             : Only ten monsters per map, runs in vanilla, gets
                          progressively harder. A semi-serious effort.

Additional Credits to   : The usual
===========================================================================
* What is included *

New levels              : 15
Sounds                  : No
Music                   : Yes
Graphics                : No
Dehacked/BEX Patch      : No
Demos                   : No
Other                   : No
Other files required    : None


* Play Information *

Game                    : Doom 2
Map #                   : MAP01-15
Single Player           : Designed for
Cooperative 2-4 Player  : No
Deathmatch 2-4 Player   : No
Other game styles       : None
Difficulty Settings     : Not implemented

* Map | Music Information *

Title screen : un30 by Bobby Prince (unused Doom midi)
Intermission : Theme song of My Little Pony : Friendship Is Magic
Text screen : Original midi by Joe-Ilya

MAP01 : Sliver of Silver | Metal by Simsam (MAP20 from Mars War)
MAP02 : Forest Harvest | Original midi by Joe-Ilya
MAP03 : The Waiting Game | Kabbalah (E2M1 from Heroes)
MAP04 : Square Ware | Lake Of Fire by Meat Puppets
MAP05 : Square Ware 2 | Second Rendez-Vous by Jean-Michel Jarre / Brian Havis
MAP06 : The Lighthouse | Original midi by Joe-Ilya
MAP07 : Crush Simple | Rape Me by Nirvana
MAP08 : The Baphomet | Polly (New Wave) by Nirvana
MAP09 : Rocket Launcher
MAP10 : Plain Plains | Babs Seed from My Little Pony : Friendship Is Magic
MAP11 : The Islands | May The Best Pet Win from My Little Pony : Friendship Is Magic
MAP12 : Offense On The Fence | Missing? Impossible! from Duke Nukem 3D
MAP13 : No Way Street | Theme song of Angry Video Game Nerd
MAP14 : Ammo Harvest | Sped up version of Countdown To Death from Doom 2
MAP15 : The End

* Construction *

Base                    : New from scratch
Build Time              : A little bit
Editor(s) used          : Doom Builder 2, Slade 3
Known Bugs              : MAP07's 667 trigger might activate twice to reveal
                          a tutti-fruity in vanilla.
Tested With             : Vanilla


* Copyright / Permissions *

This work is licensed under the Creative Commons Attribution 4.0
International License. To view a copy of this license, visit
http://creativecommons.org/licenses/by/4.0/

You are free to copy and redistribute the material in any medium or format;
and remix, transform, and build upon the material for any purpose, even
commercially. If you do so, you must give appropriate credit, provide a link
to the license, and indicate if changes were made. You may do so in any
reasonable manner, but not in any way that suggests the licensor endorses you
or your use.

* Where to get the file that this text file describes *

The Usual: ftp://archives.gamers.org/pub/idgames/ and mirrors